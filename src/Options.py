__author__ = 'Amaia Eskisabel'

from optparse import OptionParser

# moduleOptions.py


class Options:

    def __init__(self, sys.argv):
        self.argv = sys.argv

    def parse(argv):
        opts = OptionParser()
        opts.banner = "Usage: Quizzer [ options ]"
        opts.add_option("-h", "--help", "Show this message")
        opts.add_option("-f", "--file",dest="filename",help="-f Quizz.json,Assessment.json", metavar="FILE")
        opts.add_option("-m", "--file",dest="filename",help="-m Manifest.json", metavar="FILE")
        opts.add_option("-q", "--quiet",action="store_false", dest="verbose", default=True,help="don't print status messages to stdout")
        (options, args) = opts.parse_args()