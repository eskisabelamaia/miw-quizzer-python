import json

__author__ = 'Amaia Eskisabel'

# moduleFileManagement.py

class FileManagementServer:

    def parseFileJSON (self, file):
    	try:
            file = open(f, "r")
        except IOError as err:
            print("Cannot open file", file)
        else:
            strJSON = json.load(file)
            file.close()
            return strJSON