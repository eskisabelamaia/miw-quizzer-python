__author__ = 'Amaia Eskisabel'

# moduleEvaluator.py

class Evaluator:

    def evaluate(self, dataquizz, data_assessment):
        resultados = []
        result = {}
        for item in data_assessment.get("items"):
            resultado = {}
            resultado["studentId"] = item.get("studentId")
            resultado["value"] = float(self.gradeCalculation(item.get("answers"),dataquizz))
            resultados.append(resultado.copy())
        result["scores"] = resultados
        return result

    def gradeCalculation(self,answers,dataquizz):
        grade = 0
        for item in dataquizz.get("questions"):
            studentAnswer = self.findStudentAnswer(item.get("id"), answers)
            grade = self.checkQuestionType(item, studentAnswer, grade)
        return grade

    def checkQuestionType(self,item,studentAnswer, grade):
        if item.get("type") == "multichoice":
                answerOK = self.findMultichoiceAnswer (item)
                grade = self.sumGrades (studentAnswer, answerOK, grade)
        elif item.get("type") == "truefalse":
                grade = self.sumGrades (studentAnswer, item.get("correct"), grade)
        return grade

    def findMultichoiceAnswer (self, item):
        for alternative in item.get("alternatives"):
            if alternative["value"] == 1:
                return alternative["code"]

    def sumGrades (self, studentAnswer, answerOK, grade):
        if (studentAnswer == answerOK):
            grade += 1
        else:
            grade -= 0.25
        return grade

    def findStudentAnswer(self, id, answers):
        for answer in answers:
            if answer.get("question") == id:
                return answer.get("value")

    def compareScores(self, scores, calculatedScores):
        counter=0
        counterstudent = 0

        print("Comparing the scores...")
        for item in scores.get("scores"):
            counterstudent += 1
            for itemC in calculatedScores.get("scores"):
                if item.get("studentId") == itemC.get("studentId") and item.get("value") == itemC.get("value"):
                    counter += 1
        if counter == counterstudent:
            print("Calculated scores are equal")
            return True
        else:
            print("Calculated scores are not equal")
            return False