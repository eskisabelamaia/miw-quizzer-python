__author__ = 'Amaia Eskisabel'

import sys

from Evaluator import *
from FileManagement import *

class Main:

    if __name__ == "__main__":
        counter = 0
        fm = FileManagement()
        evaluator = Evaluator()
        print(sys.argv)
        if len(sys.argv) == 3:
            quizz = fm.parseFileJSON(sys.argv[1])
            assessment = fm.parseFileJSON(sys.argv[2])
            calculatedScores = evaluator.evaluate(quizz,assessment)
            fm.createFile(calculatedScores)
            print("Calculated scores has been written in /src/files/Score.json")
        if len(sys.argv) == 2:
            manifestObj = fm.readFile(sys.argv[1])
            manifest = fm.parseFileJSON(manifestObj)
            for item in manifest["tests"]:
                quizz = fm.parseUrlJSON(item["quizz"])
                assessment = fm.parseUrlJSON(item["assessment"])
                scores = fm.parseUrlJSON(item["scores"])
                calculatedScores = (evaluator.evaluate(quizz,assessment))
                print("Exam ", counter)
                evaluator.compareScores(scores,calculatedScores)
                counter += 1