import io

__author__ = 'Amaia Eskisabel'

import urllib.request
import json

# moduleFileManagement.py

class FileManagement:

    def readFile (self, promptFile):
        file = promptFile
        return file

    def parseFileJSON (self, f):
        try:
            file = open(f, "r")
        except IOError as err:
            print("Cannot open file", file)
        else:
            strJSON = json.load(file)
            file.close()
            return strJSON

    def parseUrlJSON (self, url):
        response = urllib.request.urlopen(url);
        da = response.read()
        data = json.loads(da.decode())
        return data

    def getDataFiles(self,needle, fileManifest):
        file = []
        for item in fileManifest["tests"]:
            url = self.parseUrlJSON(item["quizz"])
            file.append(url)
        return file

    def createFile (self, results):
        f1 = open("files/Scores.json", "r+")
        f1.write(json.dumps(results,sort_keys=True))
        f1.close()