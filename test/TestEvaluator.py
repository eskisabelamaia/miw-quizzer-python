__author__ = 'Amaia Eskisabel'

import unittest

from src.Evaluator import *
from src.FileManagement import *

class TestEvaluator(unittest.TestCase):

    def test_Quizzer(self):
        file = "Manifest.json"
        fm = FileManagement()
        evaluator = Evaluator()
        counter = 0
        fileManifest = fm.parseFileJSON(file)
        for item in fileManifest["tests"]:
            quizz = fm.parseUrlJSON(item["quizz"])
            assessment = fm.parseUrlJSON(item["assessment"])
            scores = fm.parseUrlJSON(item["scores"])
            calculatedScores = (evaluator.evaluate(quizz,assessment))
            print("Exam ", counter)
            evaluator.compareScores(scores,calculatedScores)
            counter += 1
        self.assertTrue(evaluator.compareScores(scores,calculatedScores))

if __name__ == '__main__':
    unittest.main()